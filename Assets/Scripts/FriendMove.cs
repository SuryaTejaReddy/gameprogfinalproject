﻿using UnityEngine;
using System.Collections;

public class FriendMove : MonoBehaviour {


     private AudioSource audioSource; // audio source for each friend

     private Transform goal; // Destination of the friend

    private bool playerFound = false; //To check if the player is found

    private bool reached = false; // If the friend is reached the destination

    private Vector3 posi; // Position of the friend

    private NavMeshAgent agent; // Nav Mesh Agent of the friend

    private int count = 0; // Count for toggling

    // Use this for initialization
    void Start()
    {
        // Setting the initial destination of the friend

         count = 0;
         agent = GetComponent<NavMeshAgent>();

        posi = new Vector3(Random.Range(0.0f, 450.0f), 4.0f, Random.Range(0.0f, 450.0f));

        audioSource = GetComponent<AudioSource>();

        agent.destination = posi;
    }

    // Update is called once per frame
    void Update()
    {
        // Check the remaining distance
        float dist = agent.remainingDistance; 
        
            if (playerFound)
        {
           
            goal = GameObject.FindGameObjectWithTag("Player").transform;
            agent.speed = 3.5f;
            agent.destination = goal.position;
        }

        else
        {
            if (dist != Mathf.Infinity && agent.pathStatus == NavMeshPathStatus.PathComplete
              && agent.remainingDistance == 0)
            {
             
                posi = new Vector3(Random.Range(0.0f, 450.0f), 1.0f, Random.Range(0.0f, 450.0f));
           
            agent.destination = posi;
            }
        }
    }

    //Handles collisions
    void OnCollisionEnter(Collision col)
    {
        // Player collided
        if (col.gameObject.tag == "Player")
        {
            if (count == 0) {
                audioSource.Play();
                count++;
            }

            playerFound = true;
        }
       
        if (col.gameObject.tag == "destroyer")
        {
            Destroy(this);
        }
    }
}
