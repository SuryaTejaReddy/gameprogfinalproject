﻿using UnityEngine;
using System.Collections;

public class LeavesScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Falling leaves particle system
            Vector3 position = new Vector3(Random.Range(0.0F, 500.0F), 0, Random.Range(0.0F, 500.0F));
            Instantiate(Resources.Load("falling leaves"), position, Quaternion.identity);
        
    }
}
