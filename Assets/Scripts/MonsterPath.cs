﻿using UnityEngine;
using System.Collections;

public class MonsterPath : MonoBehaviour {


    private Transform goal; // Goal of the zombie

    private Vector3 posi; // position of the zombie
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        // Setting goal and destination of the zombie
        goal = GameObject.FindGameObjectWithTag("Player").transform;
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = goal.position;

        // AudioSource enabling and disabling
        if (Time.timeScale == 0) {
            AudioSource audio = GetComponent<AudioSource>();
            audio.enabled = false;
        }
        else
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.enabled = true;
        }
    }
    }
