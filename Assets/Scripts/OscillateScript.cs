﻿using UnityEngine;
using System.Collections;

public class OscillateScript : MonoBehaviour {

	Vector3 original;

	// Use this for initialization
	void Start () {
		original = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.AngleAxis (Mathf.PingPong (Time.time * 50, 120), Vector3.right);
	}
}

