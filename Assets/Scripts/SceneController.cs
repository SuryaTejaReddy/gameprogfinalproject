﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

    private bool muted = false; // To mute the sound
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Uses AudioListener to mute and unmute sound
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (muted == false)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = false;
                muted = true;
            }
            else
            {
                muted = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = true;
            }
        }
    }

    // To move to the initial scene
    public void BackToMenu()
    {
        Application.LoadLevel("Introduction");
    }
}
