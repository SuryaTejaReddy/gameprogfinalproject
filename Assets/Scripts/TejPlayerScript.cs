﻿using UnityEngine;
using System.Collections;

public class TejPlayerScript : MonoBehaviour {

    private float playerX; // Player's x position
    private float playerY; // Player's y position
    private float playerZ; // Player's z position
    private Transform playerPosition; // Player's position
    private Camera main; // Camera of the scene

    // Use this for initialization

    private AudioSource audioSource;

    void Start () {

        playerPosition = transform;
        playerX = playerPosition.position.x;
        playerY = playerPosition.position.y;
        playerZ = playerPosition.position.z;

        audioSource = GetComponent<AudioSource>();    
    }
	
	// Update is called once per frame
	void Update () {

        main = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        // Zoomin and Zoomout
        if (Input.GetKey(KeyCode.V) && main.fieldOfView < 112)
        {
            main.fieldOfView += 5;
        }
        if (Input.GetKey(KeyCode.C) && main.fieldOfView > 42)
        {
            main.fieldOfView -= 5;
        }

        // Health decreases when player moves.
        if (playerX < transform.position.x || playerX > transform.position.x)
        {
            ControllerScript.healthValue -= 0.25f;
        }
        else if (playerZ < transform.position.z || playerZ > transform.position.z)
        {
            ControllerScript.healthValue -= 0.25f;
        }

        else
        {

        }

        // If health value reaches zero
        if (ControllerScript.healthValue <= 0)
        {
            Time.timeScale = 0.0f;
        }

        playerPosition = transform;
        playerX = playerPosition.position.x;
        playerY = playerPosition.position.y;
        playerZ = playerPosition.position.z;
    }

    void FixedUpdate()
    {
   
    }

    void OnCollisionEnter (Collision col)
    {
     // Handles collisions

        //Found meat
        if (col.gameObject.tag == "meat")
        {
          
            if (ControllerScript.healthValue < 960)
            {
                ControllerScript.healthValue += 25;
            }

            else if (ControllerScript.healthValue <= 1000 && ControllerScript.healthValue > 950)
            {
                ControllerScript.healthValue = 1000;
            }
            Destroy(col.gameObject);

            audioSource.Play();
        }

        //Found fruit
        if (col.gameObject.tag == "fruit")
        {
            if (ControllerScript.healthValue < 960)
            {
                ControllerScript.healthValue += 50;
            }
            else if (ControllerScript.healthValue <= 1000 && ControllerScript.healthValue > 950)
            {
                ControllerScript.healthValue = 1000;
            }
            Destroy(col.gameObject);
            audioSource.Play();
        }

        // Zombie found
        if (col.gameObject.tag == "zombie")
        {
            Time.timeScale = 0.0f;
        }
    }
}
