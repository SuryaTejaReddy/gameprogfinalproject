﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndOfGameScript : MonoBehaviour {


    private int numberOfFriends; // Number of friends saved

    public Text endOfGameComment; // Comment when reached the red beam light of castle

    private bool playerFound; // Player is near the castle, around the red beam light

    int count; // To count the number of times player is found after game ends

	// Use this for initialization
	void Start () {
        endOfGameComment.text = "";
        playerFound = false;
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 center = new Vector3(83.0f, 0.0f, 418.0f);
        float radius = 10.0f;
        Collider[] hitColliders = Physics.OverlapSphere(center, radius); // To check all the colliders around the red beam light

        for(int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject.tag == "zombie") // zombie found
            {
           
                Destroy(hitColliders[i].gameObject);
            }

            if (hitColliders[i].gameObject.tag == "Friend") //Friend Found
            {
            
                numberOfFriends++;
                hitColliders[i].gameObject.SetActive(false);
            }

            if (hitColliders[i].gameObject.tag == "Player") //Player found
            {
                playerFound = true;
            }
            else
            {
                playerFound = false;
            }
        }

        if (playerFound)
        {

            // Comment based on player and friends saved
            if (numberOfFriends == 0)
            {
                endOfGameComment.text = "Go back and save your friends";
            }
            else if (numberOfFriends == 1)
            {
                endOfGameComment.text = "You saved one. Where are your other two friends?";
            }

            else if (numberOfFriends == 2)
            {
                endOfGameComment.text = "You saved two. Grab your last friend";
            }

            else if (numberOfFriends == 3)
            {
                endOfGameComment.text = "Awesome! You win. Your score is " + ControllerScript.healthValue;
                count++;
                if (count > 20)
                {
                    Application.LoadLevel("Introduction");
                }
            }
        }
        else
        {
            endOfGameComment.text = "";
        }
    }
}
