﻿using UnityEngine;
using System.Collections;

public class DestroyerScript : MonoBehaviour {

    int friendsCount;
	// Use this for initialization
	void Start () {
        friendsCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision col)
    {

        Debug.Log("Hello Everyone");
        if (col.gameObject.tag == "Friend")
        {
            print("friend");
            friendsCount++;
        }

        if(col.gameObject.tag == "zombie")
        {
            print("zombie destroyed");
            Destroy(col.gameObject);
        }

    }

}
