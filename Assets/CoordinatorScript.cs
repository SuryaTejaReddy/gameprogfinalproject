﻿using UnityEngine;
using System.Collections;

public class CoordinatorScript : MonoBehaviour {

    private bool muted = false; // To mute the sound

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        // Muted using AudioListener
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (muted == false)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = false;
                muted = true;
            }
            else
            {
                muted = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = true;
            }
        }
    }

    // To start playing the game
   public void Play()
    {
        Application.LoadLevel("GameScene");
    }

    // To view the instructions
    public void Instructions()
    {
        Application.LoadLevel("Instructions");
    }

}
