﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllerScript : MonoBehaviour {

    public Text comment; // Comment text

    // Food prefabs to increase player's health
    private string[] food = { "meat1", "meat2", "meat3", "meat4", "Pumpkin", "Apple_Green", "Apple_Red", "Apple_Yellow", "Beet", "Cabbage", "Carrot", "Garlic", "Garlic_bunch", "Grapes", "Lemon", "Lettuce", "Onions", "Peach", "Pear_Green", "Pear_Red" };

    public Text health; // Health Text

    public static float healthValue = 1000.0f; // Value of the player's health

    private bool paused = false; // to check if paused

    private GameObject restartButton; // Restart game

    private GameObject quitButton; // Quit game

    private int count = 0; // Toggling value

    private bool muted = false; // To check if muted

    public Slider slider;

    float barDisplay  = 0;
    Vector2 pos = new Vector2(20,40);
    Vector2 size = new Vector2(60,20);
    Texture2D progressBarEmpty;
    Texture2D progressBarFull;

    // Use this for initialization
    void Start () {
        health.text = healthValue.ToString();

        slider.value = healthValue;

        float randomX = Random.Range(0.0f, 480.0f);
        float y = 1.5f;
        float randomZ = Random.Range(0.0f, 480.0f);

        for(int i = 0; i < 1000; i++)
        {
          
            Instantiate(Resources.Load(food[Random.Range(0, food.Length-1)]), new Vector3(randomX, y, randomZ), Quaternion.identity);

             randomX = Random.Range(0.0f, 480.0f);
             randomZ = Random.Range(0.0f, 480.0f);
        }

        for(int i = 0; i < 25; i++)
        {
            Instantiate(Resources.Load("monster"), new Vector3(randomX, 4.0f, randomZ), Quaternion.identity);

            randomX = Random.Range(0.0f, 480.0f);
            randomZ = Random.Range(0.0f, 480.0f);
        }

        Instantiate(Resources.Load("Friend1"), new Vector3(Random.Range(0.0f, 450.0f), 0.0f, Random.Range(0.0f, 450.0f)), Quaternion.identity) ;
        Instantiate(Resources.Load("Friend2"), new Vector3(Random.Range(0.0f, 450.0f), 0.0f, Random.Range(0.0f, 450.0f)), Quaternion.identity);
        Instantiate(Resources.Load("Friend3"), new Vector3(Random.Range(0.0f, 450.0f), 0.0f, Random.Range(0.0f, 450.0f)), Quaternion.identity);

        restartButton =  GameObject.FindGameObjectWithTag("restart");

        quitButton = GameObject.FindGameObjectWithTag("quit");

        restartButton.SetActive(false);

        quitButton.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        slider.value = healthValue;


        if (comment.text != "GAME OVER")
        {
           

            if (Input.GetKeyDown(KeyCode.M))
            {
                if (muted == false)
                {
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = false;
                    muted = true;
                }
                else
                {
                    muted = false;
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>().enabled = true;
                }
            }
            if (Input.GetKeyDown(KeyCode.P) && count % 2 == 0)
            {
                paused = true;
                Time.timeScale = 0;
                count++;
            }
            else if (Input.GetKeyDown(KeyCode.P) && count % 2 == 1)
            {
                paused = false;
                Time.timeScale = 1;
                count++;
            }
        }

        if (Time.timeScale == 0 && !paused) {
            comment.text = "GAME OVER";
            healthValue = 1000.0f;
            restartButton.SetActive(true);
            quitButton.SetActive(true);
        }

        else if (Time.timeScale == 0 && paused)
        {
            comment.text = "Paused";
        }

        else if (healthValue >= 1000)
            {
                comment.text = "Go, Find your friends";
            }

        
        else
        {
            comment.text = "";
            restartButton.SetActive(false);
            quitButton.SetActive(false);
        }

        health.text = healthValue.ToString();
      //  barDisplay = healthValue;
    }

    //void OnGUI()
    //{

    //    // draw the background:
    //    GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
    //    GUI.Box(new Rect(0, 0, size.x, size.y), progressBarEmpty);

    //    // draw the filled-in part:
    //    GUI.BeginGroup(new Rect(0, 0, size.x * (barDisplay - 100), size.y));
    //    GUI.Box(new Rect(0, 0, size.x, size.y), progressBarFull);

    //    GUI.EndGroup();

    //    GUI.EndGroup();

    //}

    public void Restart()
    {
        health.text = healthValue.ToString();
        paused = false;
        Time.timeScale = 1.0f;
        Application.LoadLevel("GameScene");
        restartButton.SetActive(false);
        quitButton.SetActive(false);
    }

    public void Quit()
    {
        health.text = healthValue.ToString();
        paused = false;
        Time.timeScale = 1.0f;
        Application.LoadLevel("Introduction");
        restartButton.SetActive(false);
        quitButton.SetActive(false);
    }
}
